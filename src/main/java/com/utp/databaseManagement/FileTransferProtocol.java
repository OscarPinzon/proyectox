/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utp.databaseManagement;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

/**
 *
 * @author Antony
 */
public class FileTransferProtocol {
      
    public void uploadPicture(String fileName, String path) {
        FTPClient client = new FTPClient();
        FileInputStream file = null;

        try {
            client.connect("ftp.panamahitek.com");
            client.login("ichang@panamahitek.com", "magi*****");
            client.setFileType(FTP.BINARY_FILE_TYPE);
            file = new FileInputStream(path);
            client.storeFile(fileName, file);
            client.logout();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (file != null) {
                    file.close();
                }
                client.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void downloadPicture() {
        // get an ftpClient object
        FTPClient client = new FTPClient();
        FileOutputStream fos = null;

        try {
            // pass directory path on server to connect
            client.connect("ftp.panamahitek.com");
            boolean login = client.login("ichang@panamahitek.com", "magi*****");
            client.setFileType(FTP.BINARY_FILE_TYPE);

            if (login) {
                System.out.println("Connection established...");

                fos = new FileOutputStream("imagen.jpg");
                boolean download = client.retrieveFile("images/test.jpg", fos);
                if (download) {
                    System.out.println("File downloaded successfully !");
                } else {
                    System.out.println("Error in downloading file !");
                }

                boolean logout = client.logout();
                if (logout) {
                    System.out.println("Connection close...");
                }
            } else {
                System.out.println("Connection fail...");
            }

        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                client.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
