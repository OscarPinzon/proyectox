package com.utp.dataManagement;

import java.util.List;

public class Marc21 {

    private int Marc21Id;
    private String tag;
    private String name;
    private String description;
    private String value;
    private boolean usage;

    public Marc21(int Marc21Id, String tag, String name, String description) {
        this.Marc21Id = Marc21Id;
        this.tag = tag;
        this.name = name;
        this.description = description;
    }

    public Marc21(boolean usage, int Marc21Id, String tag, String name, String description) {
        this.Marc21Id = Marc21Id;
        this.tag = tag;
        this.name = name;
        this.description = description;
        this.usage = usage;
    }

    public void setUsage(boolean usage) {
        this.usage = usage;
    }

    public boolean getUsage() {
        return this.usage;
    }

    public int getMarc21Id() {
        return this.Marc21Id;
    }

    public void setMarc21Id(int Marc21Id) {
        this.Marc21Id = Marc21Id;
    }

    public String getTag() {
        return this.tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String calculteTagUsage(List<DocumentType> list) {
        String binary = "";
        for (int i = list.size() - 1; i >= 0; i--) {
            binary += list.get(i).getUsage();
        }
        binary = Integer.toString(Integer.parseInt(binary, 2), 16);

        int dim = binary.length();

        if (dim < 4) {
            for (int i = 0; i < (4 - dim); i++) {
                binary = "0" + binary;
            }
        }

        return binary;
    }

}
