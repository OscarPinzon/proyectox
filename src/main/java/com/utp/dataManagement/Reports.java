package com.utp.dataManagement;

import com.utp.databaseManagement.MySQL;
import com.utp.peopleManagement.Estudiante;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

public class Reports {

    public void generateSingleStudentReport(List<Estudiante> lista) {
        try {
            JasperPrint print = JasperFillManager.fillReport("Estudiante.jasper", null, new JRBeanCollectionDataSource(lista));

            JasperExportManager.exportReportToPdfFile(print, "test.pdf");
            JasperViewer.viewReport(print, false);
        } catch (JRException ex) {
            Logger.getLogger(Reports.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
