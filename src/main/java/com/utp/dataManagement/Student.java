package com.utp.dataManagement;

public class Student {

    private String name;
    private String lastName;
    private String birthDate;
    private String bloodType;
    private String id;
    private String email;
    private String phone;
    private String cellphone;
    private String country;
    private String province;
    private String district;
    private String corregimiento;
    private String address;
    private String campus;
    private String faculty;
    private String career;
    private String rfid;
    //private String url;

    public Student(String name, String lastName, String birthDate, String bloodType, String id, String email, String phone, String cellphone, String country, String province, String district, String corregimiento, String address, String campus, String faculty, String career, String rfid/*, String url*/) {
        this.name = name;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.bloodType = bloodType;
        this.id = id;
        this.email = email;
        this.phone = phone;
        this.cellphone = cellphone;
        this.country = country;
        this.province = province;
        this.district = district;
        this.corregimiento = corregimiento;
        this.address = address;
        this.campus = campus;
        this.faculty = faculty;
        this.career = career;
        this.rfid = rfid;
        //this.url = url;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDate() {
        return this.birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getBloodType() {
        return this.bloodType;
    }

    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
        public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    public String getCellphone() {
        return this.cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }
    
    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    
    public String getProvince() {
        return this.province;
    }

    public void setProvince(String province) {
        this.province = province;
    }
    
    public String getDistrict() {
        return this.district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }
    
    public String getCorregimiento() {
        return this.corregimiento;
    }

    public void setCorregimiento(String corregimiento) {
        this.corregimiento = corregimiento;
    }
    
    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    public String getCampus() {
        return this.campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }
    
    public String getFaculty() {
        return this.faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }
    
    public String getCareer() {
        return this.career;
    }

    public void setCareer(String career) {
        this.career = career;
    }
    
    public String getRfid() {
        return this.rfid;
    }

    public void setRfid(String rfid) {
        this.rfid = rfid;
    }
    
    /*public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }*/

}
