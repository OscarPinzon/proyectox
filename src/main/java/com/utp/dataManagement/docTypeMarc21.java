/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utp.dataManagement;
public class docTypeMarc21 {
    private int typeId;
    private String marc21Id;
    private int docTypeMarc21Id;
    
    public docTypeMarc21(int typeId, String marc21Id,int docTypeMarc21Id){
        this.typeId= typeId;
        this.marc21Id= marc21Id;
        this.docTypeMarc21Id= docTypeMarc21Id;
    }
     
    public int getTypeId(){
        return this.typeId;
    }
    
    public String getMarc21Id(){
        return this.marc21Id;
    }
    
    public int getDocTypeMarc21Id(){
        return this.docTypeMarc21Id;
    }

    public void setMarc21Id(String marc21Id) {
        this.marc21Id = marc21Id;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public void setDocTypeMarc21Id(int docTypeMarc21Id) {
        this.docTypeMarc21Id = docTypeMarc21Id;
    }
    
}
