package com.utp.administrativeStructure;

public class Degree {

    private String degreeID;
    private String facultyID;
    private String degreeName;
    private String description;
    private String inProfile;
    private String outProfile;
    private Double cost;
    private String workplace;
    private int Semesters;
    private String studyMode;
    private String title;
    private int asociatedStudents;
    private int asociatedGraduates;
    private String status;

    public Degree(String degreeID, String facultyID, String degreeName, String description, String inProfile, String outProfile, Double cost, String workplace, int Semesters, String studyMode, String title, int asociatedStudents, int asociatedGraduates, String status) {
        this.degreeID = degreeID;
        this.facultyID = facultyID;
        this.degreeName = degreeName;
        this.description = description;
        this.inProfile = inProfile;
        this.outProfile = outProfile;
        this.cost = cost;
        this.workplace = workplace;
        this.Semesters = Semesters;
        this.studyMode = studyMode;
        this.title = title;
        this.asociatedStudents = asociatedStudents;
        this.asociatedGraduates = asociatedGraduates;
        this.status = status;
    }

    public String getDegreeID() {
        return this.degreeID;
    }

    public void setDegreeID(String degreeID) {
        this.degreeID = degreeID;
    }

    public String getFacultyID() {
        return this.facultyID;
    }

    public void setFacultyID(String facultyID) {
        this.facultyID = facultyID;
    }

    public String getDegreeName() {
        return this.degreeName;
    }

    public void setDegreeName(String degreeName) {
        this.degreeName = degreeName;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInProfile() {
        return this.inProfile;
    }

    public void setInProfile(String inProfile) {
        this.inProfile = inProfile;
    }

    public String getOutProfile() {
        return this.outProfile;
    }

    public void setOutProfile(String outProfile) {
        this.outProfile = outProfile;
    }

    public Double getCost() {
        return this.cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public String getWorkplace() {
        return this.workplace;
    }

    public void setWorkplace(String workplace) {
        this.workplace = workplace;
    }

    public int getSemesters() {
        return this.Semesters;
    }

    public void setSemesters(int Semesters) {
        this.Semesters = Semesters;
    }

    public String getStudyMode() {
        return this.studyMode;
    }

    public void setStudyMode(String studyMode) {
        this.studyMode = studyMode;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getAsociatedStudents() {
        return this.asociatedStudents;
    }

    public void setAsociatedStudents(int asociatedStudents) {
        this.asociatedStudents = asociatedStudents;
    }

    public int getAsociatedGraduates() {
        return this.asociatedGraduates;
    }

    public void setAsociatedGraduates(int asociatedGraduates) {
        this.asociatedGraduates = asociatedGraduates;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
