package com.utp.proyectoX;

import com.utp.dataManagement.Marc21;
import com.utp.databaseManagement.FileTransferProtocol;
import com.utp.databaseManagement.PHP;
import com.utp.interfaces.mainWindow;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.UIManager;

public class ProyectoX {

    public static void main(String[] args) {

        try {

            JFrame.setDefaultLookAndFeelDecorated(true);

            //UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
            //UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            //UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
        } catch (Exception e) {
            e.printStackTrace();
        }
       new mainWindow().setVisible(true);

    }

}
