package com.utp.interfaces;

import javax.swing.border.Border;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.border.Border;

public class BackgroundImage implements Border {

    public BufferedImage back;
 
    public BackgroundImage(int ancho, int alto) {
    
        try {
            URL imagePath = new URL(getClass().getResource("/images/MAGI_main.png").toString());
            back = ImageIO.read(imagePath);
        } catch (Exception ex) {

        }
    }

    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        g.drawImage(back, 0, 0, null);
    }

    public Insets getBorderInsets(Component c) {
        return new Insets(0, 0, 0, 0);
    }

    public boolean isBorderOpaque() {
        return false;
    }

}
