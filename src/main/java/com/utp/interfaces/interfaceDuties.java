package com.utp.interfaces;

import com.utp.databaseManagement.FileTransferProtocol;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.util.Calendar;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JRadioButton;

public class interfaceDuties extends JDesktopPane {

    public class Image extends javax.swing.JPanel {

        String path;

        public Image(String path) {
            this.path = path;
            this.setSize(300, 400); //se selecciona el tamaño del panel
        }

//Se crea un método cuyo parámetro debe ser un objeto Graphics
        @Override
        public void paint(Graphics grafico) {
            Dimension height = getSize();

//Se selecciona la imagen que tenemos en el paquete de la //ruta del programa
            ImageIcon Img = new ImageIcon(getClass().getResource(path));

//se dibuja la imagen que tenemos en el paquete Images //dentro de un panel
            grafico.drawImage(Img.getImage(), 0, 0, height.width, height.height, null);

            setOpaque(false);
            super.paintComponent(grafico);
        }
    }

    private int ancho = java.awt.Toolkit.getDefaultToolkit().getScreenSize().width;
    private int alto = java.awt.Toolkit.getDefaultToolkit().getScreenSize().height;

    public void setScreenSize(JFrame frame) {
        frame.setBounds(0, 0, ancho, alto - 40);
    }

    public void setBackGroundImage(JDesktopPane pane) {
        pane.setBorder(new BackgroundImage(ancho, alto - 40));
    }

    public void radioButtonState(JRadioButton input, boolean state) {
        input.setSelected(state);
    }

    public void fillYears(JComboBox box) {
        box.removeAllItems();
        Calendar c = Calendar.getInstance();
        box.addItem("");
        for (int i = 0; i <= (c.get(Calendar.YEAR)) - 1950; i++) {
            box.addItem(i + 1950);
        }
    }

    public String loadImage() {
        JFileChooser fileChooser = new JFileChooser();
        int result = fileChooser.showOpenDialog(null);
        if (result == JFileChooser.APPROVE_OPTION) {
            return fileChooser.getSelectedFile().getAbsolutePath();
        } else {
            return null;
        }
    }

    public void onFacultySelect(String choice, JComboBox box) {
        System.out.println(choice);
        switch (choice) {
            case "Ingeniería Eléctrica":
                box.removeAllItems();
                box.addItem("Licenciatura en Ingeniería Electromecánica");
                box.addItem("Licenciatura en Ingeniería Eléctrica y Electrónica");
                box.addItem("Licenciatura en Ingeniería Electrónica y Telecomunicaciones");
                box.addItem("Licenciatura en Sistemas Eléctricos y Automatización");
                box.addItem("Licenciatura en Electrónica y Sistemas de Comunicación");
                box.addItem("Licenciatura en Electrónica Digital y Control Automático");

                break;

            case "Ingeniería Mecánica":
                box.removeAllItems();
                box.addItem("Licenciatura en Ingeniería Mecánica");
                box.addItem("Licenciatura en Ingeniería de Mantenimiento");
                box.addItem("Licenciatura en Ingeniería Naval");
                box.addItem("Licenciatura en Ingeniería Aeronáutica");
                box.addItem("Licenciatura en Ingeniería de Energía y Ambiente");
                box.addItem("Licenciatura en Mecánica Industrial");
                box.addItem("Licenciatura en Refrigeración y Aire Acondicionado");
                box.addItem("Licenciatura en Mecánica Automotriz");
                box.addItem("Licenciatura en Soldadura");
                box.addItem("Licenciatura en Administración de Aviación con Opción a Vuelo (Piloto)");
                box.addItem("Licenciatura en Administración de Aviación");
                box.addItem("Técnico en Despacho de Vuelo");
                box.addItem("Técnico en Ingeniería de Mantenimiento de Aeronaves con especialización en Motores y Fuselaje");

                break;

            case "Ingeniería Civil":

                box.removeAllItems();
                box.addItem("Licenciatura en Ingeniería Civil");
                box.addItem("Licenciatura en Topografía Licenciatura en Edificaciones");
                box.addItem("Licenciatura En Dibujo Automatizado");
                box.addItem("Licenciatura en Ingeniería Geomática");
                box.addItem("Licenciatura en Ingeniería Marítima Portuaria");
                box.addItem("Licenciatura en Operaciones Marítimas y Portuarias");
                box.addItem("Licenciatura en Ingeniería Geológica");
                box.addItem("Licenciatura en Ingeniería Ambiental");
                box.addItem("Licenciatura en Saneamiento y Ambiente");

                break;

            case "Ingeniería Industrial":

                box.removeAllItems();

                box.addItem("Licenciatura en Ingeniería Industrial");
                box.addItem("Licenciatura en Gestión Administrativa");
                box.addItem("Licenciatura en Ingeniería Mecánica Industrial");
                box.addItem("Licenciatura en Logística y Transporte Multimodal");
                box.addItem("Licenciatura en Gestión de la Producción Industrial");
                box.addItem("Licenciatura en Mercadeo y Comercio Internacional");
                box.addItem("Licenciatura en Recursos Humanos y Gestión de la Productividad");

                break;

            case "Ingeniería en Sistemas Computacionales":

                box.removeAllItems();

                box.addItem("Licenciatura en Ingeniería de Sistemas de Información");
                box.addItem("Licenciatura en Ingeniería de Sistemas y Computación");
                box.addItem("Licenciatura en Ingeniería de Software");
                box.addItem("Licenciatura en Desarrollo de Software");
                box.addItem("Licenciatura en Informática Aplicada a la Educación");
                box.addItem("Licenciatura en Redes Informáticas");
                box.addItem("Técnico en Informática para la Gestión Empresarial");

                break;

            case "Ciencias y Tecnologías":

                box.removeAllItems();
                box.addItem("Licenciatura en Comunicación Ejecutiva Bilingüe");
                box.addItem("Licenciatura en Ingeniería Forestal");
                box.addItem("Licenciatura en Ingeniería en Alimentos");
                break;

        }

    }

    public void onProvinceSelect(String choice, JComboBox box) {
        System.out.println(choice);
        switch (choice) {
            case "Bocas Del Toro":
                box.removeAllItems();
                box.addItem("Bocas Del Toro");
                box.addItem("Changuinola");
                box.addItem("Chiriquí Grande");
                break;

            case "Panamá":
                box.removeAllItems();
                box.addItem("Panamá");
                box.addItem("Chepo");
                box.addItem("Balboa");
                box.addItem("Chimán");
                box.addItem("San Miguelito");
                box.addItem("Taboga");
                break;

            case "Chiriquí":
                box.removeAllItems();
                box.addItem("David");
                box.addItem("Alanje");
                box.addItem("Barú");
                box.addItem("Boquerón");
                box.addItem("Boquete");
                box.addItem("Bugaba");
                box.addItem("Dolega");
                box.addItem("Gualaca");
                box.addItem("Remedios");
                box.addItem("Renacimiento");
                box.addItem("San Félix");
                box.addItem("San Lorenzo");
                box.addItem("Tolé");
                break;

            case "Coclé":
                box.removeAllItems();
                box.addItem("Aguadulce");
                box.addItem("Antón");
                box.addItem("La Pintada");
                box.addItem("Natá");
                box.addItem("Olá");
                box.addItem("Penonomé");
                break;

            case "Colón":
                box.removeAllItems();
                box.addItem("Colón");
                box.addItem("Chagres");
                box.addItem("Donoso");
                box.addItem("Portobelo");
                box.addItem("Santa Isabel");
                break;

            case "Darién":
                box.removeAllItems();
                box.addItem("Chepigana");
                box.addItem("Pinogana");
                break;
            case "Chitré":
                box.removeAllItems();
                box.addItem("Las Minas");
                box.addItem("Los Pozos");
                box.addItem("Ocú");
                box.addItem("Parita");
                box.addItem("Pesé");
                box.addItem("Santa María");
                box.addItem("Chitré");
                break;
            case "Los Santos":
                box.removeAllItems();
                box.addItem("Las Tablas");
                box.addItem("Guararé");
                box.addItem("Los Santos");
                box.addItem("Macaracas");
                box.addItem("Pedasí");
                box.addItem("Pocír");
                box.addItem("Tonosí");
                break;

            case "Panamá Oeste":
                box.removeAllItems();
                box.addItem("Arraiján");
                box.addItem("La Chorrera");
                box.addItem("Capira");
                box.addItem("Chame");
                box.addItem("San Carlos");
                break;

            case "Veraguas":
                box.removeAllItems();
                box.addItem("Santiago");
                box.addItem("Atalaya");
                box.addItem("Calobre");
                box.addItem("Cañazas");
                box.addItem("La Mesa");
                box.addItem("Las Palmas");
                box.addItem("Mariato");
                box.addItem("Montijo");
                box.addItem("Río de Jesús");
                box.addItem("San Francisco");
                box.addItem("Santa Fe");
                box.addItem("Soná");
                break;

            case "Guna Yala":
                box.removeAllItems();
                box.addItem("Guna Yala");
                break;

            case "Comarca Ngöbe-Buglé":
                box.removeAllItems();
                box.addItem("Besikó");
                box.addItem("Jirondai");
                box.addItem("Kankintú");
                box.addItem("Kusapín");
                box.addItem("Mironó");
                box.addItem("Müna");
                box.addItem("Nole Duima");
                box.addItem("Ñürüm");
                box.addItem("Santa Catalina o Calovébora");
                break;

            case "Comarca Emberá-Wounaan":
                box.removeAllItems();
                box.addItem("Cémaco");
                box.addItem("Sambú");
                break;

        }

    }

    public void onDistrictSelect(String choice, JComboBox box) {

        switch (choice) {

            case "Panamá":
                box.removeAllItems();
                box.addItem("Ciudad de Panamá");
                box.addItem("Ancón");
                box.addItem("Chilibre");
                box.addItem("Las Cumbres");
                box.addItem("Pacora");
                box.addItem("San Martín Tocumen");
                box.addItem("Las Mañanitas");
                box.addItem("24 de Diciembre");
                box.addItem("Alcalde Díaz");
                box.addItem("Ernesto Córdoba Campos. En la Ciudad de Panamá se encuentran las siguientes subdivisones: San Felipe");
                box.addItem("El Chorrillo");
                box.addItem("Santa Ana");
                box.addItem("La Exposición o Calidonia");
                box.addItem("Curundú");
                box.addItem("Betania");
                box.addItem("Bella Vista");
                box.addItem("Pueblo Nuevo");
                box.addItem("San Francisco");
                box.addItem("Parque Lefevre");
                box.addItem("Río Abajo");
                box.addItem("Juan Díaz");
                box.addItem("Pedregal");
                break;

            case "Balboa":
                box.removeAllItems();
                box.addItem("San Miguel");
                box.addItem("La Ensenada");
                box.addItem("La Esmeralda");
                box.addItem("La Guinea");
                box.addItem("Pedro González");
                box.addItem("Saboga");
                break;

            case "Chepo":
                box.removeAllItems();
                box.addItem("Chepo");
                box.addItem("Cañita");
                box.addItem("Chepillo");
                box.addItem("El Llano");
                box.addItem("Las Margaritas");
                box.addItem("Santa Cruz de Chinina");
                box.addItem("Comara Kuna de Madungandi");
                box.addItem("Tortí");
                break;

            case "Chimán":
                box.removeAllItems();
                box.addItem("Chimán");
                box.addItem("Brujas");
                box.addItem("Gonzalo Vásquez");
                box.addItem("Pásiga");
                box.addItem("Unión Santeña");
                break;

            case "San Miguelito":
                box.removeAllItems();
                box.addItem("Amelia Denis de Icaza");
                box.addItem("Belisario Porras");
                box.addItem("José Domingo Espinar");
                box.addItem("Mateo Iturralde");
                box.addItem("Victoriano Lorenzo");
                box.addItem("Arnulfo Arias");
                box.addItem("Belisario Frías");
                box.addItem("Omar Torrijos");
                box.addItem("Rufina Alfaro");
                break;

            case "Taboga":
                box.removeAllItems();
                box.addItem("Taboga");
                box.addItem("Otoque Oriente");
                box.addItem("Otoque Occidente");
                break;

            case "Bocas Del Toro":
                box.removeAllItems();
                box.addItem("Bocas del Toro");
                box.addItem("Bastimentos");
                box.addItem("Cauchero");
                box.addItem("Punta Laurel");
                box.addItem("Tierra Oscura");
                break;
            case "Changuinola":
                box.removeAllItems();
                box.addItem("Chanquinola");
                box.addItem("Almirante");
                box.addItem("Guabito");
                box.addItem("Teribe");
                box.addItem("Valle del Risco");
                box.addItem("El Empalme");
                box.addItem("Las Tablas");
                box.addItem("Cochigró");
                box.addItem("La Gloria");
                box.addItem("Las Delicias");
                box.addItem("Nance del Risco");
                box.addItem("Valle de Agua Arriba");
                break;

            case "Chiriquí Grande":
                box.removeAllItems();
                box.addItem("Chiriquí Grande");
                box.addItem("Miramar");
                box.addItem("Punta Peña");
                box.addItem("Punta Robalo");
                box.addItem("Rambala");
                box.addItem("Bajo Cedro");
                break;

            case "David":
                box.removeAllItems();
                box.addItem("David");
                box.addItem("Bijagual");
                box.addItem("Cochea");
                box.addItem("Chiriquí");
                box.addItem("Guacá");
                box.addItem("Las Lomas");
                box.addItem("Pedregal");
                box.addItem("San Carlos");
                box.addItem("San Pablo Nuevo");
                box.addItem("San Pablo Viejo");
                break;

            case "Alanje":
                box.removeAllItems();
                box.addItem("Alanje");
                box.addItem("Divalá");
                box.addItem("El Tejar");
                box.addItem("Guarumal");
                box.addItem("Palo Grande");
                box.addItem("Querévalo");
                box.addItem("Santo Tomás");
                box.addItem("Canta Gallo");
                box.addItem("Nuevo México");
                break;
            case "Barú":
                box.removeAllItems();
                box.addItem("Puerto Amuelles");
                box.addItem("Limones");
                box.addItem("Progreso");
                box.addItem("Baco");
                box.addItem("Rodolfo Aguilar Delgado");
                break;
            case "Boquerón":
                box.removeAllItems();
                box.addItem("Boquerón");
                box.addItem("Bágala");
                box.addItem("cordillera");
                box.addItem("Guabal");
                box.addItem("Guayabal");
                box.addItem("Paraíso");
                box.addItem("Pedregal");
                box.addItem("Tijeras");
                break;
            case "Boquete":
                box.removeAllItems();
                box.addItem("Bajo Boquete");
                box.addItem("Caldera");
                box.addItem("Palmira");
                box.addItem("Alto Boquete");
                box.addItem("Jaramillo");
                box.addItem("Los Naranjos");
                break;
            case "Bugaba":
                box.removeAllItems();
                box.addItem("La Concepción");
                box.addItem("Aserrío de Gariché");
                box.addItem("Bugaba");
                box.addItem("Cerro Punta");
                box.addItem("Gómez");
                box.addItem("La Estrella");
                box.addItem("San Andrés");
                box.addItem("Santa Marta");
                box.addItem("Santa Rosa");
                box.addItem("Santo Domingo");
                box.addItem("Sortová");
                box.addItem("Volcán");
                box.addItem("El Bongo");
                break;

            case "Dolega":
                box.removeAllItems();
                box.addItem("olega");
                box.addItem("Dos Ríos");
                box.addItem("Los Anastacios");
                box.addItem("Potrerillos");
                box.addItem("Potrerillos Abajo");
                box.addItem("Rovira");
                box.addItem("Tinajas");
                box.addItem("Los Algarrobos");
                break;

            case "Gualaca":
                box.removeAllItems();
                box.addItem("Gualaca");
                box.addItem("Hornito");
                box.addItem("Los Ángeles");
                box.addItem("Paja de Sombrero");
                box.addItem("Rincón");
                break;
            case "Remedios":
                box.removeAllItems();
                box.addItem("Remedios");
                box.addItem("El Nancito");
                box.addItem("El Porvenir");
                box.addItem("El Puerto");
                box.addItem("Santa Lucía");
                break;

            case "Renacimiento":
                box.removeAllItems();
                box.addItem("Río Sereno");
                box.addItem("Breñón");
                box.addItem("Cañas Gordas");
                box.addItem("Monte Lirio");
                box.addItem("Plaza Caisán");
                box.addItem("Santa Cruz");
                box.addItem("Dominical");
                box.addItem("Santa Clara");
                break;

            case "San Féliz":
                box.removeAllItems();
                box.addItem("Las Lajas");
                box.addItem("Juay");
                box.addItem("Lajas Adentro");
                box.addItem("San Félix");
                box.addItem("Santa Cruz");
                break;
            case "San Lorenzo":
                box.removeAllItems();
                box.addItem("Horconcitos");
                box.addItem("Boca Chica");
                box.addItem("Boca del Monte");
                box.addItem("San Juan");
                box.addItem("San Lorenzo");
                break;
            case "Tolé":
                box.removeAllItems();
                box.addItem("Tolé");
                box.addItem("Bella Vista");
                box.addItem("Cerro Viejo");
                box.addItem("El Cristo");
                box.addItem("Justo Fidel Palacios");
                box.addItem("Lajas de Tolé");
                box.addItem("Potrero de Caña");
                box.addItem("Quebrada de Piedra");
                box.addItem("Veladero");
                break;

            case "Penonomé":
                box.removeAllItems();
                box.addItem("Penonomé");
                box.addItem("Cañaveral");
                box.addItem("Coclé");
                box.addItem("Chiguirí Arriba");
                box.addItem("El Coco");
                box.addItem("Pajonal");
                box.addItem("Río Grande");
                box.addItem("Río Indio");
                box.addItem("Toabré");
                box.addItem("Tulú");
                break;

            case "Aguadulce":
                box.removeAllItems();
                box.addItem("Aguadulce");
                box.addItem("El Cristo");
                box.addItem("El Roble");
                box.addItem("Pocrí");
                box.addItem("Barrios Unidos");
                break;

            case "Antón":
                box.removeAllItems();
                box.addItem("Antón");
                box.addItem("Cabuya");
                box.addItem("El Chirú");
                box.addItem("El Retiro");
                box.addItem("El Valle");
                box.addItem("Juan Díaz");
                box.addItem("Río Hato");
                box.addItem("San Juan de Dios");
                box.addItem("Santa Rita");
                box.addItem("Caballero");
                break;

            case "La Pintada":
                box.removeAllItems();
                box.addItem("La Pintada");
                box.addItem("El Harino");
                box.addItem("El Potrero");
                box.addItem("Llano Grande");
                box.addItem("Piedras Gordas");
                box.addItem("Las Lomas");
                break;
            case "Natá":
                box.removeAllItems();
                box.addItem("Natá");
                box.addItem("Capellanía");
                box.addItem("El Caño");
                box.addItem("Guzmán");
                box.addItem("Las Huacas");
                box.addItem("Toza");
                break;
            case "Olá":
                box.removeAllItems();
                box.addItem("Olá");
                box.addItem("El Copé");
                box.addItem("El Palmar");
                box.addItem("El Picacho");
                box.addItem("La Pava");
                break;

            case "Colón":
                box.removeAllItems();
                box.addItem("Colón");
                box.addItem("Buena Vista");
                box.addItem("Cativá");
                box.addItem("Ciricito");
                box.addItem("Cristóbal");
                box.addItem("Escobal");
                box.addItem("Limón");
                box.addItem("Nueva Providencia");
                box.addItem("Puerto Pilón");
                box.addItem("Sabanitas");
                box.addItem("Salamanca");
                box.addItem("San Juan");
                box.addItem("Santa Rosa");
                break;

            case "Chagres":
                box.removeAllItems();
                box.addItem("Nuevo Chagres");
                box.addItem("Achiote");
                box.addItem("El Guabo");
                box.addItem("La Encantada");
                box.addItem("Palmas Bellas");
                box.addItem("Piña");
                box.addItem("Salud");
                break;

            case "Donoso":
                box.removeAllItems();
                box.addItem("Miguel de la Borda");
                box.addItem("Coclé del Norte");
                box.addItem("El Guásimo");
                box.addItem("Gobea");
                box.addItem("Río Indio");
                box.addItem("San José del General");
                break;
            case "Portobelo":
                box.removeAllItems();
                box.addItem("Portobelo");
                box.addItem("Cacique");
                box.addItem("Puerto Lindo o Garrote");
                box.addItem("Isla Grande");
                box.addItem("María Chiquita");
                break;

            case "Santa Isabel":
                box.removeAllItems();
                box.addItem("Palenque");
                box.addItem("Cuango");
                box.addItem("Miramar");
                box.addItem("Nombre de Dios");
                box.addItem("Palmira");
                box.addItem("Playa Chiquita");
                box.addItem("Santa Isabel");
                box.addItem("Viento Frío");
                break;

            case "Chepigana":
                box.removeAllItems();
                box.addItem("La Palma");
                box.addItem("Camogantí");
                box.addItem("Chepigana");
                box.addItem("Garachiné");
                box.addItem("Jaqué");
                box.addItem("Puerto Piña");
                box.addItem("Río Congo");
                box.addItem("Río Iglesias");
                box.addItem("Sambú");
                box.addItem("Setegantí");
                box.addItem("Taimatí");
                box.addItem("Tucutí");
                box.addItem("Agua Fría");
                box.addItem("Cucunatí");
                box.addItem("Río Congo Arriba");
                box.addItem("Santa Fe");
                break;
            case "Pinogana":
                box.removeAllItems();
                box.addItem("El Real de Santa María");
                box.addItem("Boca de Cupé");
                box.addItem("Paya");
                box.addItem("Pinogana");
                box.addItem("Púcuro");
                box.addItem("Yape");
                box.addItem("Yaviza");
                box.addItem("Metetí");
                box.addItem("Comarca Kuna de Wargandí");
                break;
            case "Chitré":
                box.removeAllItems();
                box.addItem("Chitré");
                box.addItem("La Arena");
                box.addItem("Monagrillo");
                box.addItem("Llano Bonito");
                box.addItem("San Juan Bautista");
                break;
            case "Las Minas":
                box.removeAllItems();
                box.addItem("Las Minas");
                box.addItem("Chepo");
                box.addItem("Chumical");
                box.addItem("El Toro");
                box.addItem("Leones");
                box.addItem("Quebrada del Rosario");
                box.addItem("Quebrada El Ciprián");
                break;
            case "Los Pozos":
                box.removeAllItems();
                box.addItem("Los Pozos");
                box.addItem("Capurí");
                box.addItem("El Calabacito");
                box.addItem("El Cedro");
                box.addItem("La Arena");
                box.addItem("La Pitaloza");
                box.addItem("Los Cerritos");
                box.addItem("Los Cerros de Paja");
                box.addItem("Las Llanas");
                break;
            case "Ocú":
                box.removeAllItems();
                box.addItem("Ocú");
                box.addItem("Cerro Largo");
                box.addItem("Los Llanos");
                box.addItem("Llano Grande");
                box.addItem("Peñas Chatas");
                box.addItem("El Tijera");
                box.addItem("Menchac");
                break;
            case "Parita":
                box.removeAllItems();
                box.addItem("Parita");
                box.addItem("Cabuya");
                box.addItem("Los Castillos");
                box.addItem("Llano de la Cruz");
                box.addItem("París");
                box.addItem("Portobelillo");
                box.addItem("Potuga");
                break;
            case "Pesé":
                box.removeAllItems();
                box.addItem("Pesé");
                box.addItem("Las Cabras");
                box.addItem("El Pájaro");
                box.addItem("El Barrero");
                box.addItem("El Pedregoso");
                box.addItem("El Ciruelo");
                box.addItem("Sabanagrande");
                box.addItem("Rincón Hondo");
                break;

            case "Santa María":
                box.removeAllItems();
                box.addItem("Santa María");
                box.addItem("Chupampa");
                box.addItem("El Rincón");
                box.addItem("El Limón");
                box.addItem("Los Canelos");
                break;

            case "Las Tablas":
                box.removeAllItems();
                box.addItem("Las Tablas");
                box.addItem("Bajo Corral");
                box.addItem("Bayano");
                box.addItem("El Carate");
                box.addItem("El Cocal");
                box.addItem("El Manantial");
                box.addItem("El Muñoz");
                box.addItem("El Pedregoso");
                box.addItem("La Laja");
                box.addItem("La Miel");
                box.addItem("La Palma");
                box.addItem("La Tiza");
                box.addItem("Las Palmitas");
                box.addItem("Las Tablas Abajo");
                box.addItem("Nuario");
                box.addItem("Palmira");
                box.addItem("Peña Blanca");
                box.addItem("Río Hondo");
                box.addItem("San José");
                box.addItem("San Miguel");
                box.addItem("Santo Domingo");
                box.addItem("Sesteadero");
                box.addItem("Valle Rico");
                box.addItem("Vallerriquito");
                break;
            case "Guararé":
                box.removeAllItems();
                box.addItem("Guararé");
                box.addItem("El Espinal");
                box.addItem("El Macano");
                box.addItem("Guararé Arriba");
                box.addItem("La Enea");
                box.addItem("La Pasera");
                box.addItem("Las Trancas");
                box.addItem("Llano Abajo");
                box.addItem("El Hato");
                box.addItem("Perales");
                break;
            case "Los Santos":
                box.removeAllItems();
                box.addItem("La Villa de los Santos");
                box.addItem("El Guásimo");
                box.addItem("La Colorada");
                box.addItem("La Espigadilla");
                box.addItem("Las Cruces");
                box.addItem("Las Guabas");
                box.addItem("Los Ángeles");
                box.addItem("Los Olivos");
                box.addItem("Llano Largo");
                box.addItem("Sabanagrande");
                box.addItem("Santa Ana");
                box.addItem("Tres Quebradas");
                box.addItem("Agua Buena");
                box.addItem("Villa Lourdes");
                break;
            case "Macaracas":
                box.removeAllItems();
                box.addItem("Macaracas");
                box.addItem("Bahía Honda");
                box.addItem("Bajos de Güera");
                box.addItem("Corozal");
                box.addItem("Chupá");
                box.addItem("El Cedro");
                box.addItem("Espino Amarillo");
                box.addItem("La Mesa");
                box.addItem("Las Palmas");
                box.addItem("Llano de Piedra");
                box.addItem("Mogollón");
                break;

            case "Pedasí":
                box.removeAllItems();
                box.addItem("Pedasí");
                box.addItem("Los Asientos");
                box.addItem("Mariabé");
                box.addItem("Purio");
                box.addItem("Oria Arriba");
                break;

            case "Pocrí":
                box.removeAllItems();
                box.addItem("Pocrí");
                box.addItem("El Cañafístulo");
                box.addItem("Lajamina");
                box.addItem("Paraíso");
                box.addItem("Paritilla");
                break;

            case "Tonosí":
                box.removeAllItems();
                box.addItem("Tonosí");
                box.addItem("Altos de Güera");
                box.addItem("Cañas");
                box.addItem("El Bebedero");
                box.addItem("El Cacao");
                box.addItem("El Cortezo");
                box.addItem("Flores");
                box.addItem("Guánico");
                box.addItem("Tronosa");
                box.addItem("Cambutal");
                box.addItem("Isla de Cañas");
                break;

            case "La Chorrera":
                box.removeAllItems();
                box.addItem("La Chorrera");
                box.addItem("Amador");
                box.addItem("Arosemena");
                box.addItem("El Arado");
                box.addItem("El Coco");
                box.addItem("Feuillet");
                box.addItem("Guadalupe");
                box.addItem("Herrera");
                box.addItem("Hurtado");
                box.addItem("Iturralde");
                box.addItem("La Represa");
                box.addItem("Los Díaz");
                box.addItem("Mendoza");
                box.addItem("Obaldía");
                box.addItem("Playa Leona");
                box.addItem("Puerto Caimito");
                box.addItem("Santa Rita");
                break;
            case "Arraiján":
                box.removeAllItems();
                box.addItem("Arraiján");
                box.addItem("Juan Demóstenes Arosemena");
                box.addItem("Nuevo Emperador");
                box.addItem("Santa Clara");
                box.addItem("Veracruz");
                box.addItem("Vista Alegre");
                box.addItem("Burunga");
                box.addItem("Cerro Silvestre");
                break;

            case "Capira":
                box.removeAllItems();
                box.addItem("Capira");
                box.addItem("Caimito");
                box.addItem("Campana");
                box.addItem("Cermeño");
                box.addItem("Cirí de los Sotos");
                box.addItem("Cirí Grande");
                box.addItem("El Cacao");
                box.addItem("La Trinidad");
                box.addItem("Las Ollas Arriba");
                box.addItem("Lídice");
                box.addItem("Villa Carmen");
                box.addItem("Villa Rosario");
                box.addItem("Santa Rosa");
                break;

            case "Chame":
                box.removeAllItems();
                box.addItem("Chame");
                box.addItem("Bejuco");
                box.addItem("Buenos Aires");
                box.addItem("Cabuya");
                box.addItem("Chicá");
                box.addItem("El Líbano");
                box.addItem("Las Lajas");
                box.addItem("Nueva Gorgona");
                box.addItem("Punta Chame");
                box.addItem("Sajalices");
                box.addItem("Sorá");
                break;

            case "San Carlos":
                box.removeAllItems();
                box.addItem("San Carlos");
                box.addItem("El Espino");
                box.addItem("El Higo");
                box.addItem("Guayabito");
                box.addItem("La Ermita");
                box.addItem("La Laguna");
                box.addItem("Las Uvas");
                box.addItem("Los Llanitos");
                box.addItem("San José");
                break;

            case "Santiago":
                box.removeAllItems();
                box.addItem("Santiago");
                box.addItem("La Colorada");
                box.addItem("La Peña");
                box.addItem("La Raya de Santa María");
                box.addItem("Ponuga");
                box.addItem("San Pedro del Espino");
                box.addItem("Canto del Llano");
                box.addItem("Los Algarrobos");
                box.addItem("Carlos Santana Ávila");
                box.addItem("Edwin Fábrega");
                box.addItem("San Martín de Porres");
                box.addItem("Urracá");
                break;
            case "Atalaya":
                box.removeAllItems();
                box.addItem("Atalaya");
                box.addItem("El Barrito");
                box.addItem("La Montañuela");
                box.addItem("La Carrillo");
                box.addItem("San Antonio");
                break;
            case "Calobre":
                box.removeAllItems();
                box.addItem("Calobre");
                box.addItem("Barnizal");
                box.addItem("Chitra");
                box.addItem("El Cocla");
                box.addItem("El Potrero");
                box.addItem("La Laguna");
                box.addItem("La Raya de Calobre");
                box.addItem("La Tetilla");
                box.addItem("La Yeguada");
                box.addItem("Las Guías");
                box.addItem("Monjarás");
                box.addItem("San José");
                break;
            case "Cañazas":
                box.removeAllItems();
                box.addItem("Cañazas");
                box.addItem("Cerro de Plata");
                box.addItem("El Picador");
                box.addItem("Los Valles");
                box.addItem("San José");
                box.addItem("San Marcelo");
                box.addItem("El Aromillo");
                box.addItem("Las Crces");
                break;

            case "La Mesa":
                box.removeAllItems();
                box.addItem("La Mesa");
                box.addItem("Bisvalles");
                box.addItem("Boró");
                box.addItem("Llano Grande");
                box.addItem("San Bartolo");
                box.addItem("Los Milagros");
                break;

            case "Las Palmas":
                box.removeAllItems();
                box.addItem("Las Palmas son Las Palmas");
                box.addItem("Cerro de Casa");
                box.addItem("Corozal");
                box.addItem("El María");
                box.addItem("El Prado");
                box.addItem("El Rincón");
                box.addItem("Lolá");
                box.addItem("Pixvae");
                box.addItem("Puerto Vidal");
                box.addItem("San Martín de Porres");
                box.addItem("Viguí");
                box.addItem("Zapotillo");
                break;

            case "Mariato":
                box.removeAllItems();
                box.addItem("Llano de Catival o Mariato");
                box.addItem("El Cacao");
                box.addItem("Quebro");
                box.addItem("Tebario");
                break;
            case "Montijo":
                box.removeAllItems();
                box.addItem("Montijo");
                box.addItem("Gobernadora");
                box.addItem("La Garceana");
                box.addItem("Leones");
                box.addItem("Pilón");
                box.addItem("Cébaco");
                box.addItem("Costa Hermosa");
                box.addItem("Unión del Norte");
                break;

            case "Río de Jesús":
                box.removeAllItems();
                box.addItem("Río de Jesús");
                box.addItem("Las Huacas");
                box.addItem("Los Castillos");
                box.addItem("Utira");
                box.addItem("Catorce de Noviembre");
                break;

            case "San Francisco":
                box.removeAllItems();
                box.addItem("San Francisco");
                box.addItem("Corral Falso");
                box.addItem("Los Hatillos");
                box.addItem("Remance");
                box.addItem("San Juan");
                box.addItem("San José");
                break;
            case "Santa Fe":
                box.removeAllItems();
                box.addItem("Santa Fe");
                box.addItem("Calovébora");
                box.addItem("El Alto");
                box.addItem("El Cuay");
                box.addItem("El Pantano");
                box.addItem("Gatú o Gatucito");
                box.addItem("Río Luis");
                box.addItem("Rubén Cantú");
                break;

            case "Soná":
                box.removeAllItems();
                box.addItem("Soná");
                box.addItem("Bahía Honda");
                box.addItem("Calidonia");
                box.addItem("Cativé");
                box.addItem("El Marañón");
                box.addItem("Guarumal");
                box.addItem("La Soledad");
                box.addItem("Quebrada de Oro");
                box.addItem("Río Grande");
                box.addItem("Rodeo Viejo");
                break;

            case "Comarca Guna Yala":
                box.removeAllItems();
                box.addItem("Narganá");
                box.addItem("Ailigandí");
                box.addItem("Puerto Obaldía");
                box.addItem("Tubualá");
                break;

            case "Besikó":
                box.removeAllItems();
                box.addItem("Soloy");
                box.addItem("Boca de Balsa");
                box.addItem("Camarón Arriba");
                box.addItem("Cerro Banco");
                box.addItem("Cerro de Patena");
                box.addItem("Emplanada de Chorcha");
                box.addItem("Nämnoni");
                box.addItem("Niba");
                break;

            case "Jirondai":
                box.removeAllItems();
                box.addItem("Gwaribiara");
                box.addItem("Büri");
                box.addItem("Tu gwai");
                box.addItem("Man Creek");
                box.addItem("Samboa");
                break;

            case "Kankintú":
                box.removeAllItems();
                box.addItem("Bisira");
                box.addItem("Kankintú");
                box.addItem("Gworoni");
                box.addItem("Mününi");
                box.addItem("Piedra Roja");
                box.addItem("Tolote");
                box.addItem("Calante");
                break;

            case "Kusapín":
                box.removeAllItems();
                box.addItem("Bahía Azul");
                box.addItem("Kusapin");
                box.addItem("Tobobe");
                box.addItem("Cañaveral");
                box.addItem("Río Chiriquí");
                break;

            case "Mironó":
                box.removeAllItems();
                box.addItem("Hato Pilón");
                box.addItem("Cascabel");
                box.addItem("Hato Corotú");
                box.addItem("Hato Culantro");
                box.addItem("Hato Jobo");
                box.addItem("Hato Julí");
                box.addItem("Quebrada de Loro");
                box.addItem("Salto Dupí");
                break;

            case "Müna":
                box.removeAllItems();
                box.addItem("Chichica");
                box.addItem("Alto Caballero");
                box.addItem("Bakama");
                box.addItem("Cerro Caña");
                box.addItem("Cerro Puerco");
                box.addItem("Krüa");
                box.addItem("Maraca");
                box.addItem("Nibra");
                box.addItem("Peña Blanca");
                box.addItem("Roka");
                box.addItem("Sitio Prado");
                box.addItem("Umani");
                break;

            case "Nole Duima":
                box.removeAllItems();
                box.addItem("Cerro Iglesias");
                box.addItem("Hato Chamí");
                box.addItem("Jädaberi");
                box.addItem("Lajero");
                box.addItem("Susama");
                break;

            case "Ñürüm":
                box.removeAllItems();
                box.addItem("Buenos Aires");
                box.addItem("Agua de Salud");
                box.addItem("Alto de Jesús");
                box.addItem("Cerro Pelado");
                box.addItem("El Bale");
                box.addItem("El Paredón");
                box.addItem("El Piro");
                box.addItem("Guayabito");
                box.addItem("Güibale");
                break;

            case "Sta. Catalina o Calovébora":
                box.removeAllItems();
                box.addItem("Santa Catalina o Calovébora (Bledeshia)");
                box.addItem("Valle Bonito (Dogata)");
                box.addItem("Loma Yuca (Ijuicho)");
                box.addItem("San Pedrito (Jiküi)");
                box.addItem("Alto Bilingüe (Gdogüeshia)");
                break;

            case "Cémaco":
                box.removeAllItems();
                box.addItem("Cirilo Guainora");
                box.addItem("Lajas Blancas");
                box.addItem("Manuel Ortega");
                break;

            case "Sambú":
                box.removeAllItems();
                box.addItem("Río Sábalo");
                box.addItem("Jingurudó");
                break;

        }

    }

}
