
package com.utp.interfaces;

import com.utp.databaseManagement.MySQL;

public class jInternalFrameBusqueda extends javax.swing.JFrame {


    public jInternalFrameBusqueda() {
        initComponents();
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jInternalFrameBusc = new javax.swing.JInternalFrame();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jRadioButtonPersona = new javax.swing.JRadioButton();
        jRadioButtonDocumento = new javax.swing.JRadioButton();
        jPanelPersona = new javax.swing.JPanel();
        jComboBoxBSeleccion = new javax.swing.JComboBox();
        jTextFieldBusqueda = new javax.swing.JTextField();
        jRadioButtonBEstudiante = new javax.swing.JRadioButton();
        jRadioButtonBDocente = new javax.swing.JRadioButton();
        jComboBoxBusqueda = new javax.swing.JComboBox();
        jComboBoxBDocumento = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jInternalFrameBusc.setTitle("Consulta");
        jInternalFrameBusc.setMinimumSize(new java.awt.Dimension(595, 432));
        jInternalFrameBusc.setVisible(true);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        jLabel1.setText("Busqueda");

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton1.setText("Buscar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jRadioButtonPersona.setSelected(true);
        jRadioButtonPersona.setText("Persona");
        jRadioButtonPersona.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonPersonaActionPerformed(evt);
            }
        });

        jRadioButtonDocumento.setText("Documento");
        jRadioButtonDocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonDocumentoActionPerformed(evt);
            }
        });

        jPanelPersona.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jComboBoxBSeleccion.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Nombre", "Cédula", "Facultad", "Carrera", "Turno", "Año" }));
        jComboBoxBSeleccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxBSeleccionActionPerformed(evt);
            }
        });

        jTextFieldBusqueda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldBusquedaActionPerformed(evt);
            }
        });

        jRadioButtonBEstudiante.setText("Estudiante");
        jRadioButtonBEstudiante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonBEstudianteActionPerformed(evt);
            }
        });

        jRadioButtonBDocente.setText("Docente");
        jRadioButtonBDocente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonBDocenteActionPerformed(evt);
            }
        });

        jComboBoxBusqueda.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jComboBoxBDocumento.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboBoxBDocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxBDocumentoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelPersonaLayout = new javax.swing.GroupLayout(jPanelPersona);
        jPanelPersona.setLayout(jPanelPersonaLayout);
        jPanelPersonaLayout.setHorizontalGroup(
            jPanelPersonaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPersonaLayout.createSequentialGroup()
                .addGap(81, 81, 81)
                .addComponent(jRadioButtonBEstudiante)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jRadioButtonBDocente)
                .addGap(111, 111, 111))
            .addGroup(jPanelPersonaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelPersonaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jComboBoxBDocumento, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jComboBoxBSeleccion, javax.swing.GroupLayout.Alignment.LEADING, 0, 148, Short.MAX_VALUE))
                .addGap(26, 26, 26)
                .addComponent(jTextFieldBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jComboBoxBusqueda, 0, 181, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanelPersonaLayout.setVerticalGroup(
            jPanelPersonaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPersonaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelPersonaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButtonBEstudiante)
                    .addComponent(jRadioButtonBDocente))
                .addGap(31, 31, 31)
                .addGroup(jPanelPersonaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBoxBSeleccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBoxBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jComboBoxBDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(161, Short.MAX_VALUE))
        );

        jTextFieldBusqueda.setVisible(false);
        jComboBoxBusqueda.setVisible(false);
        jComboBoxBDocumento.setVisible(false);

        javax.swing.GroupLayout jInternalFrameBuscLayout = new javax.swing.GroupLayout(jInternalFrameBusc.getContentPane());
        jInternalFrameBusc.getContentPane().setLayout(jInternalFrameBuscLayout);
        jInternalFrameBuscLayout.setHorizontalGroup(
            jInternalFrameBuscLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jInternalFrameBuscLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jInternalFrameBuscLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jInternalFrameBuscLayout.createSequentialGroup()
                        .addGap(134, 134, 134)
                        .addComponent(jRadioButtonPersona)
                        .addGap(140, 140, 140)
                        .addComponent(jRadioButtonDocumento)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jInternalFrameBuscLayout.createSequentialGroup()
                        .addGroup(jInternalFrameBuscLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanelPersona, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jInternalFrameBuscLayout.createSequentialGroup()
                                .addGroup(jInternalFrameBuscLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())))
        );
        jInternalFrameBuscLayout.setVerticalGroup(
            jInternalFrameBuscLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jInternalFrameBuscLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jInternalFrameBuscLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButtonPersona)
                    .addComponent(jRadioButtonDocumento))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanelPersona, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jInternalFrameBusc, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jInternalFrameBusc, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jRadioButtonPersonaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonPersonaActionPerformed
        //Limpia el panel
        jComboBoxBDocumento.setVisible(false);
        jTextFieldBusqueda.setVisible(false);
        
        //Seleccion de la busqueda para Persona
        jRadioButtonDocumento.setSelected(false);
        jRadioButtonPersona.setSelected(true);
        if(jRadioButtonPersona.isSelected()){
            jPanelPersona.setVisible(true);
            
            jRadioButtonBDocente.setSelected(false);
            jRadioButtonBEstudiante.setSelected(false);
            jRadioButtonBEstudiante.setVisible(true);
            jRadioButtonBDocente.setVisible(true);
            
            jComboBoxBSeleccion.setVisible(false);
        }
        else {
        }
    }//GEN-LAST:event_jRadioButtonPersonaActionPerformed

    private void jComboBoxBSeleccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxBSeleccionActionPerformed
        //Lista de las opciones de busqueda por estudiante
        String valor = String.valueOf(jComboBoxBSeleccion.getSelectedItem());
        switch (valor){
            //Seleccion de Estudiante y Docente
            case "Nombre":
                jTextFieldBusqueda.setVisible(true);
                jComboBoxBusqueda.setVisible(false);
                break;
            case "Cédula":
                jTextFieldBusqueda.setVisible(true);
                jComboBoxBusqueda.setVisible(false);
                break;
            case "Pasaporte":
                jTextFieldBusqueda.setVisible(true);
                jComboBoxBusqueda.setVisible(false);
                break;
            case "Facultad":
                jComboBoxBusqueda.removeAllItems();
                jComboBoxBusqueda.addItem("Ingeniería Eléctrica");
                jComboBoxBusqueda.addItem("Ingeniería Mecánica");
                jComboBoxBusqueda.addItem("Ingeniería Civil");
                jComboBoxBusqueda.addItem("Ingeniería en Sistemas Computacionales");
                jComboBoxBusqueda.addItem("Ingeniería Industrial");
                jComboBoxBusqueda.addItem("Ciencias y Tecnologías");
                jComboBoxBusqueda.setVisible(true);
                jTextFieldBusqueda.setVisible(false);
                break;
            case "Carrera":
                jComboBoxBusqueda.removeAllItems();
                jComboBoxBusqueda.addItem("Licenciatura en Ingeniería Electromecánica");
                jComboBoxBusqueda.addItem("Licenciatura en Ingeniería Eléctrica y Electrónica");
                jComboBoxBusqueda.addItem("Licenciatura en Ingeniería Electrónica y Telecomunicaciones");
                jComboBoxBusqueda.addItem("Licenciatura en Sistemas Eléctricos y Automatización");
                jComboBoxBusqueda.addItem("Licenciatura en Electrónica y Sistemas de Comunicación");
                jComboBoxBusqueda.addItem("Licenciatura en Electrónica Digital y Control Automático");
                jComboBoxBusqueda.addItem("Licenciatura en Ingeniería Mecánica");
                jComboBoxBusqueda.addItem("Licenciatura en Ingeniería de Mantenimiento");
                jComboBoxBusqueda.addItem("Licenciatura en Ingeniería Naval");
                jComboBoxBusqueda.addItem("Licenciatura en Ingeniería Aeronáutica");
                jComboBoxBusqueda.addItem("Licenciatura en Ingeniería de Energía y Ambiente");
                jComboBoxBusqueda.addItem("Licenciatura en Mecánica Industrial");
                jComboBoxBusqueda.addItem("Licenciatura en Refrigeración y Aire Acondicionado");
                jComboBoxBusqueda.addItem("Licenciatura en Mecánica Automotriz");
                jComboBoxBusqueda.addItem("Licenciatura en Soldadura");
                jComboBoxBusqueda.addItem("Licenciatura en Administración de Aviación con Opción a Vuelo (Piloto)");
                jComboBoxBusqueda.addItem("Licenciatura en Administración de Aviación");
                jComboBoxBusqueda.addItem("Técnico en Despacho de Vuelo");
                jComboBoxBusqueda.addItem("Técnico en Ingeniería de Mantenimiento de Aeronaves con especialización en Motores y Fuselaje");
                jComboBoxBusqueda.addItem("Licenciatura en Ingeniería Civil");
                jComboBoxBusqueda.addItem("Licenciatura en Topografía Licenciatura en Edificaciones");
                jComboBoxBusqueda.addItem("Licenciatura En Dibujo Automatizado");
                jComboBoxBusqueda.addItem("Licenciatura en Ingeniería Geomática");
                jComboBoxBusqueda.addItem("Licenciatura en Ingeniería Marítima Portuaria");
                jComboBoxBusqueda.addItem("Licenciatura en Operaciones Marítimas y Portuarias");
                jComboBoxBusqueda.addItem("Licenciatura en Ingeniería Geológica");
                jComboBoxBusqueda.addItem("Licenciatura en Ingeniería Ambiental");
                jComboBoxBusqueda.addItem("Licenciatura en Saneamiento y Ambiente");
                jComboBoxBusqueda.addItem("Licenciatura en Ingeniería Industrial");
                jComboBoxBusqueda.addItem("Licenciatura en Gestión Administrativa");
                jComboBoxBusqueda.addItem("Licenciatura en Ingeniería Mecánica Industrial");
                jComboBoxBusqueda.addItem("Licenciatura en Logística y Transporte Multimodal");
                jComboBoxBusqueda.addItem("Licenciatura en Gestión de la Producción Industrial");
                jComboBoxBusqueda.addItem("Licenciatura en Mercadeo y Comercio Internacional");
                jComboBoxBusqueda.addItem("Licenciatura en Recursos Humanos y Gestión de la Productividad");
                jComboBoxBusqueda.addItem("Licenciatura en Ingeniería de Sistemas de Información");
                jComboBoxBusqueda.addItem("Licenciatura en Ingeniería de Sistemas y Computación");
                jComboBoxBusqueda.addItem("Licenciatura en Ingeniería de Software");
                jComboBoxBusqueda.addItem("Licenciatura en Desarrollo de Software");
                jComboBoxBusqueda.addItem("Licenciatura en Informática Aplicada a la Educación");
                jComboBoxBusqueda.addItem("Licenciatura en Redes Informáticas");
                jComboBoxBusqueda.addItem("Técnico en Informática para la Gestión Empresarial");
                jComboBoxBusqueda.addItem("Licenciatura en Comunicación Ejecutiva Bilingüe");
                jComboBoxBusqueda.addItem("Licenciatura en Ingeniería Forestal");
                jComboBoxBusqueda.addItem("Licenciatura en Ingeniería en Alimentos");
                jComboBoxBusqueda.setVisible(true);
                jTextFieldBusqueda.setVisible(false);
                break;
            case "Sede":
                jComboBoxBusqueda.removeAllItems();
                jComboBoxBusqueda.addItem("Campus Dr. Levis Sasso");
                jComboBoxBusqueda.addItem("Howard");
                jComboBoxBusqueda.addItem("Tocumen");
                jComboBoxBusqueda.addItem("Azuero");
                jComboBoxBusqueda.addItem("Bocas del Toro");
                jComboBoxBusqueda.addItem("Chiriquí");
                jComboBoxBusqueda.addItem("Coclé");
                jComboBoxBusqueda.addItem("Colón");
                jComboBoxBusqueda.addItem("Panamá Oeste");
                jComboBoxBusqueda.addItem("Veraguas");
                jComboBoxBusqueda.setVisible(true);
                jTextFieldBusqueda.setVisible(false);
                break;
            case "Provincia":
                jComboBoxBusqueda.removeAllItems();
                jComboBoxBusqueda.addItem("Panamá");
                jComboBoxBusqueda.addItem("Bocas del Toro");
                jComboBoxBusqueda.addItem("Chiriquí");
                jComboBoxBusqueda.addItem("Veraguas");
                jComboBoxBusqueda.addItem("Coclé");
                jComboBoxBusqueda.addItem("Colón");
                jComboBoxBusqueda.addItem("Los Santos");
                jComboBoxBusqueda.addItem("Herrera");
                jComboBoxBusqueda.addItem("Panamá Oeste");
                jComboBoxBusqueda.addItem("Darién");
                jComboBoxBusqueda.addItem("Guna Yala");
                jComboBoxBusqueda.addItem("Comarca Ngöbe-Buglé");
                jComboBoxBusqueda.addItem("Comarca Emberá-Wounaan");
                jComboBoxBusqueda.setVisible(true);
                jTextFieldBusqueda.setVisible(false);
                break;
            case "Año":
                jTextFieldBusqueda.setVisible(true);
                jComboBoxBusqueda.setVisible(false);
                break;
                
            //Selecciones de Documento
            case "Libro":
                jComboBoxBDocumento.removeAllItems();
                jComboBoxBDocumento.addItem("Titulo");
                jComboBoxBDocumento.addItem("Autor");
                jComboBoxBDocumento.addItem("Editorial");
                jComboBoxBDocumento.addItem("Año");
                jComboBoxBDocumento.setVisible(true);
                break;
            case "Folleto":
                jComboBoxBDocumento.removeAllItems();
                jComboBoxBDocumento.addItem("Titulo");
                jComboBoxBDocumento.addItem("Año");
                jComboBoxBDocumento.setVisible(true);
                break;
            case "Tesis":
                jComboBoxBDocumento.removeAllItems();
                jComboBoxBDocumento.addItem("Titulo");
                jComboBoxBDocumento.addItem("Año");
                jComboBoxBDocumento.setVisible(true);
                break;
        }
    }//GEN-LAST:event_jComboBoxBSeleccionActionPerformed

    private void jRadioButtonBEstudianteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonBEstudianteActionPerformed
        jRadioButtonBDocente.setSelected(false);
        jRadioButtonBEstudiante.setSelected(true);
        
        //Colocar las opciones de busqueda para estudiante dentro del ComboBox
        if (jRadioButtonBEstudiante.isSelected()){
            jComboBoxBSeleccion.removeAllItems();
                jComboBoxBSeleccion.addItem("Nombre");
                jComboBoxBSeleccion.addItem("Cédula");
                jComboBoxBSeleccion.addItem("Pasaporte");
                jComboBoxBSeleccion.addItem("Facultad");
                jComboBoxBSeleccion.addItem("Carrera");
                jComboBoxBSeleccion.addItem("Sede");
                jComboBoxBSeleccion.addItem("Provincia");
                jComboBoxBSeleccion.addItem("Año");
            jComboBoxBSeleccion.setVisible(true);

        }
        else {
        }
    }//GEN-LAST:event_jRadioButtonBEstudianteActionPerformed

    private void jTextFieldBusquedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldBusquedaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldBusquedaActionPerformed

    private void jRadioButtonDocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonDocumentoActionPerformed
        //Limpia el panel
        jComboBoxBDocumento.setVisible(false);
        jTextFieldBusqueda.setVisible(false);
        jComboBoxBusqueda.setVisible(false);
        
        //Seleccion de la busqueda para Documento
        jRadioButtonPersona.setSelected(false);
        jRadioButtonDocumento.setSelected(true);
        if(jRadioButtonDocumento.isSelected()){
            jPanelPersona.setVisible(true);
            jRadioButtonBEstudiante.setVisible(false);
            jRadioButtonBDocente.setVisible(false);
            
            jComboBoxBSeleccion.removeAllItems();
                jComboBoxBSeleccion.addItem("CD");
                jComboBoxBSeleccion.addItem("Folleto");
                jComboBoxBSeleccion.addItem("Hemeroteca");
                jComboBoxBSeleccion.addItem("Libro");
                jComboBoxBSeleccion.addItem("Material Literario");
                jComboBoxBSeleccion.addItem("Monografía");
                jComboBoxBSeleccion.addItem("Multimedia");
                jComboBoxBSeleccion.addItem("Práctica Profesional");
                jComboBoxBSeleccion.addItem("Referencia");
                jComboBoxBSeleccion.addItem("Tesis");
                jComboBoxBSeleccion.addItem("Tesis Doctorado");
                jComboBoxBSeleccion.addItem("Tesis Maestría");
                jComboBoxBSeleccion.addItem("Tesis PostGrado");
                jComboBoxBSeleccion.addItem("Videos");
            jComboBoxBSeleccion.setVisible(true);
        }
        else {
        }
    }//GEN-LAST:event_jRadioButtonDocumentoActionPerformed

    private void jRadioButtonBDocenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonBDocenteActionPerformed
        jRadioButtonBEstudiante.setSelected(false);
        jRadioButtonBDocente.setSelected(true);
        
        //Colocar las opciones de busqueda para docente dentro del ComboBox
        if (jRadioButtonBDocente.isSelected()){
            jComboBoxBSeleccion.removeAllItems();
                jComboBoxBSeleccion.addItem("Nombre");
                jComboBoxBSeleccion.addItem("Cédula");
                jComboBoxBSeleccion.addItem("Pasaporte");
                jComboBoxBSeleccion.addItem("Facultad");
                jComboBoxBSeleccion.addItem("Carrera");
                jComboBoxBSeleccion.addItem("Sede");
                jComboBoxBSeleccion.addItem("Provincia");
                jComboBoxBSeleccion.addItem("Año");
            jComboBoxBSeleccion.setVisible(true);

        }
        else {
        }
    }//GEN-LAST:event_jRadioButtonBDocenteActionPerformed

    private void jComboBoxBDocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxBDocumentoActionPerformed
        String valor = String.valueOf(jComboBoxBDocumento.getSelectedItem());
        switch (valor){
            case "Titulo":
                jTextFieldBusqueda.setVisible(true);
                break;
            case "Autor":
                jTextFieldBusqueda.setVisible(true);
                break;
            case "Editorial":
                jTextFieldBusqueda.setVisible(true);
                break;
            case "Año":
                jTextFieldBusqueda.setVisible(true);
                break;
        }
    }//GEN-LAST:event_jComboBoxBDocumentoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(jInternalFrameBusqueda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(jInternalFrameBusqueda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(jInternalFrameBusqueda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(jInternalFrameBusqueda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new jInternalFrameBusqueda().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox jComboBoxBDocumento;
    private javax.swing.JComboBox jComboBoxBSeleccion;
    private javax.swing.JComboBox jComboBoxBusqueda;
    private javax.swing.JInternalFrame jInternalFrameBusc;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanelPersona;
    private javax.swing.JRadioButton jRadioButtonBDocente;
    private javax.swing.JRadioButton jRadioButtonBEstudiante;
    private javax.swing.JRadioButton jRadioButtonDocumento;
    private javax.swing.JRadioButton jRadioButtonPersona;
    private javax.swing.JTextField jTextFieldBusqueda;
    // End of variables declaration//GEN-END:variables
}
